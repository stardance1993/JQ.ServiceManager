﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using JQ.ServiceManager.Base;
using JQ.ServiceManager.Base.Interfaces;
using JQ.ServiceManager.Base.Models;
using JQ.ServiceManager.Base.Repos;
using JQ.ServiceManager.misc;

namespace JQ.ServiceManager
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            JQ.ServiceManager.misc.NinjectContainer.NinjectCore.Bind<IRepo<ServiceBase>>().To<ServiceBaseRepo>().InSingletonScope();
            JQ.ServiceManager.misc.NinjectContainer.NinjectCore.Bind<IRepo<SysParam>>().To<SysParamRepo>().InSingletonScope();
            JQ.ServiceManager.misc.NinjectContainer.NinjectCore.Bind<IRepo<Log>>().To<LogRepo>().InSingletonScope();
            JQ.ServiceManager.misc.NinjectContainer.NinjectCore.Bind<LogApi>().ToSelf().InSingletonScope();

            //System.Diagnostics.Process process = new System.Diagnostics.Process();
            //process.StartInfo.FileName = "JQ.PACS.Update.Cli.exe";   
            //process.StartInfo.Arguments = "update exec -d JQServerManager";
            //process.Start();
        }
    }
}
