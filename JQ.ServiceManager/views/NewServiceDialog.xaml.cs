﻿using JQ.ServiceManager.Base.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JQ.ServiceManager.views
{
    /// <summary>
    /// NewServiceDialog.xaml 的交互逻辑
    /// </summary>
    public partial class NewServiceDialog : System.Windows.Controls.UserControl
    {
        public NewServiceDialog()
        {
            InitializeComponent();
        }

        public Action<string, string, string> OnServiceFileConfirm;

        public Action<ServiceManagementObject> OnServiceDetailConfirm;

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string name = tbCustomName.Text;
            string path = tbServiceFilePath.Text;
            string isWatch = string.Empty;
            if(cbIsWatch.SelectedItem != null)
            {
                isWatch = cbIsWatch.Text;
            }
            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(path))
            {
                OnServiceFileConfirm?.Invoke(name,path,isWatch); 
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string selectedFile = string.Empty;
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "应用程序(*.exe)|*.exe";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                selectedFile = ofd.FileName;
                tbServiceFilePath.Text = selectedFile;
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            System.Windows.Window winref = new System.Windows.Window();

            UCServiceView servView = new UCServiceView();
            servView.OnServiceSelected += (p) =>
            {
                OnServiceDetailConfirm?.Invoke(p);
            };
            UIManager.NewDialog("服务浏览",  servView, out winref, 600, 500);
        }
    }
}
