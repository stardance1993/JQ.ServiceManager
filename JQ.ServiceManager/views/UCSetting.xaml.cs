﻿using JQ.ServiceManager.Base.Interfaces;
using JQ.ServiceManager.Base.Models;
using Ninject;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JQ.ServiceManager.views
{
    /// <summary>
    /// UCSetting.xaml 的交互逻辑
    /// </summary>
    public partial class UCSetting : UserControl
    {
        List<string> changedList;

        List<SysParam> historyData;

        public UCSetting()
        {
            InitializeComponent();
            this.DataContext = this;
            //this.Loaded += UCSetting_Loaded;
            UCSetting_Loaded(null, null);
        }

        private void UCSetting_Loaded(object sender, RoutedEventArgs e)
        {
            changedList = new List<string>();
            historyData = new List<SysParam>();
            ParamList = new ObservableCollection<SysParam>();
            var paramList = JQ.ServiceManager.misc.NinjectContainer.NinjectCore.Get<IRepo<SysParam>>().Get(it => 1 == 1);
            foreach(var par in paramList)
            {
                ParamList.Add(par);

                historyData.Add(new SysParam
                {
                    ID = par.ID,
                    Key = par.Key,
                    Value = par.Value,
                    Group = par.Group,
                    Note = par.Note
                });
            }
        }

        private ObservableCollection<SysParam> _params;

        public ObservableCollection<SysParam> ParamList
        {
            get { return _params; }
            set { _params = value; }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            foreach(var item in ParamList)
            {
                SysParam rawData = historyData.Single(it => it.ID == item.ID);
                if(!rawData.Value.Equals(item.Value) || !rawData.Group.Equals(item.Group) || !rawData.Note.Equals(item.Note))
                {
                    JQ.ServiceManager.misc.NinjectContainer.NinjectCore.Get<IRepo<SysParam>>().Update(item);
                }
            }
        }
    }
}
