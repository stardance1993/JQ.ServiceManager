﻿using HandyControl.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using Window = System.Windows.Window;

namespace JQ.ServiceManager.views
{
    public class UIManager
    {
        public static void NewDialog(string title, System.Windows.Controls.UserControl uc, out Window winref, int width = 0,int height = 0)
        {
            System.Windows.Window win = new System.Windows.Window();
            win.Width = width != 0 ? width : 600;
            win.Height = height != 0 ? height : 350;
            win.Content = uc;
            win.Title = title;
            win.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            win.ResizeMode = ResizeMode.NoResize;
            winref = win;
            win.ShowDialog();
        }


        public static void NewInfo(string msg)
        {
            HandyControl.Controls.MessageBox.Info(msg, "提示");
        }


        public static MessageBoxResult NewAsk(string msg)
        {
            return HandyControl.Controls.MessageBox.Ask(msg, "警告");
        }
    }
}
