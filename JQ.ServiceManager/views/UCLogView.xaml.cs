﻿using JQ.ServiceManager.Base;
using JQ.ServiceManager.Base.Interfaces;
using JQ.ServiceManager.Base.Models;
using Ninject;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JQ.ServiceManager.views
{
    /// <summary>
    /// UCLogView.xaml 的交互逻辑
    /// </summary>
    public partial class UCLogView : UserControl
    {
        private ObservableCollection<Log> logs;

        public ObservableCollection<Log> Logs
        {
            get { return logs; }
            set { logs = value; }
        }

        private string selectedRange;

        public string SelectedRange
        {
            get { return selectedRange; }
            set 
            { 
                selectedRange = value; 
            }
        }



        public UCLogView()
        {
            InitializeComponent();
            Load();
            this.DataContext = this;
        }

        private void Load()
        {
            Logs = new ObservableCollection<Log>();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var ss = searchRange.Text;
            DateTime serachDateCriteria = new DateTime();
            switch(ss)
            {
                case "最近一天":
                    serachDateCriteria = DateTime.Now.AddDays(-1);
                    break;
                case "最近一周":
                    serachDateCriteria = DateTime.Now.AddDays(-7);
                    break;
                case "最近一月":
                    serachDateCriteria = DateTime.Now.AddDays(-30);
                    break;
            }
            var results = JQ.ServiceManager.misc.NinjectContainer.NinjectCore.Get<IRepo<Log>>().Get(it => it.CreateTime > serachDateCriteria);
            if(results != null && results.Any())
            {
                Logs.Clear();
                foreach (var log in results)
                {
                    Logs.Add(log);
                }
            }
            
        }
    }
}
