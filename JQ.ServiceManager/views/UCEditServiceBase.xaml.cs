﻿using JQ.ServiceManager.Base.Interfaces;
using JQ.ServiceManager.Base.Models;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JQ.ServiceManager.views
{
    /// <summary>
    /// UCEditServiceBase.xaml 的交互逻辑
    /// </summary>
    public partial class UCEditServiceBase : UserControl
    {
        private ServiceBase currentServ;

        public ServiceBase CurrentServ
        {
            get { return currentServ; }
            set { currentServ = value; }
        }

        public Action OnModifyServicePropertyCompleted { get; set; }


        public UCEditServiceBase(ServiceBase sb)
        {
            InitializeComponent();
            CurrentServ = sb;
            switch (CurrentServ.IsWatch)
            {
                case "是":
                    cbIsWatch.Text = "是";
                    break;
                case "否":
                    cbIsWatch.Text = "否";
                    break;
            }

            this.DataContext = this;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            switch(cbIsWatch.Text)
            {
                case "是":
                    CurrentServ.IsWatch = "是";
                    break;
                case "否":
                default:
                    CurrentServ.IsWatch = "否";
                    break;
            }
            JQ.ServiceManager.misc.NinjectContainer.NinjectCore.Get<IRepo<ServiceBase>>().Update(CurrentServ);
            OnModifyServicePropertyCompleted?.Invoke();
        }
    }
}
