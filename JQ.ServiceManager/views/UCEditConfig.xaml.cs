﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JQ.ServiceManager.views
{
    /// <summary>
    /// UCEditConfig.xaml 的交互逻辑
    /// </summary>
    public partial class UCEditConfig : UserControl
    {

        private ObservableCollection<StatusSettings> statusSettings;

        public ObservableCollection<StatusSettings> AppSettings
        {
            get { return statusSettings; }
            set { statusSettings = value; }
        }

        public Action<Dictionary<string,string>> OnModifySetting { get; set; }



        public UCEditConfig(Dictionary<string, string> appSettings)
        {
            InitializeComponent();
            this.DataContext = this;
            AppSettings = new ObservableCollection<StatusSettings>();
            foreach (var keyValuePair in appSettings)
            {
                AppSettings.Add(new StatusSettings
                {
                    Key = keyValuePair.Key,
                    Value = keyValuePair.Value,
                    Status = 0,
                    rawVal = keyValuePair.Value,
                });
            }
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string,string> statusSettDic = new Dictionary<string, string>();
            int modifyCount = 0;
            foreach(var setting in AppSettings)
            {
                if(setting.Status == 1)
                {
                    modifyCount++;
                }
                statusSettDic.Add( setting.Key, setting.Value );
            }
            UIManager.NewInfo($"本次修改{modifyCount}处内容");
            OnModifySetting?.Invoke(statusSettDic);
            (this.Parent as Window).Close();
        }
    }

    public class StatusSettings:ObservableObject
    {
        public string Key { get; set; }

        private string _value;
        public string Value
        {
            get
            {
                return _value;
            }
            set
            {
                if (rawVal != null)
                {
                    if (!value.Equals(rawVal))
                    {
                        Status = 1;
                    }
                    else
                    {
                        Status = 0;
                    }
                }
                _value = value;
            }
        }

        public string rawVal { get; set; }

        private int status;

        public int Status
        {
            get { return status; }
            set
            {
                status = value;
                RaisePropertyChanged(nameof(Status));
            }
        }
    }
}
