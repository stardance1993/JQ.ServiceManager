﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JQ.ServiceManager.misc
{
    public class NinjectContainer
    {
        public static IKernel ninjectKernel;

        public static IKernel NinjectCore
        {
            get
            {
                if(ninjectKernel == null)
                {
                    ninjectKernel = new StandardKernel();
                }
                return ninjectKernel;
            }
        }

    }
}
