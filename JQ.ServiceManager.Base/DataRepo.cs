﻿using JQ.ServiceManager.Base.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JQ.ServiceManager.Base
{
    public class DataRepo
    {
        private LogApi log = new LogApi();

        public List<ServiceBase> GetAll()
        {
            var context = DataBaseContext.NewContext;
            List<ServiceBase> ret = new List<ServiceBase>();
            try
            {
                //DataTable dt = context.Ado.GetDataTable("Select * from ServiceBase;");
                ret = context.Queryable<ServiceBase>().ToList();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex.StackTrace);
                throw;
            }
             
            return ret;
            
        }

        public void Add(ServiceBase serviceItem)
        {
            var context = DataBaseContext.NewContext;
            int rows = context.Insertable<ServiceBase>(serviceItem).ExecuteCommand();
            if(rows > 0)
            {
                log.Info($"新增服务:{serviceItem.CustomName}-{serviceItem.ServiceFilePath}-{serviceItem.IsWatch}",null);
            }
        }

        public void Delete(ServiceBase item)
        {
            var context = DataBaseContext.NewContext;
            context.Deleteable<ServiceBase>(item).ExecuteCommand();
        }

        public void Update(ServiceBase item)
        {
            var context = DataBaseContext.NewContext;
            context.Updateable<ServiceBase>(item).ExecuteCommand();
        }
    }
}
