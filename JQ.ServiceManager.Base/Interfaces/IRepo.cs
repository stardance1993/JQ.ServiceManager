﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace JQ.ServiceManager.Base.Interfaces
{
    public interface IRepo<T>
    {
        List<T> Get(Expression<Func<T, bool>> expression);

        void Update(T t);

        void Insert(T t);

        void Delete(T t);

    }
}
