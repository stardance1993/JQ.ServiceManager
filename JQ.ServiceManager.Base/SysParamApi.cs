﻿using JQ.ServiceManager.Base.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JQ.ServiceManager.Base
{
    public class SysParamApi
    {
        private static Dictionary<string, string> _sysParams;

        public static Dictionary<string,string> SysParams
        {
            get
            {
                if (_sysParams == null || _sysParams.Count == 0)
                {
                    var dbContext = DataBaseContext.NewContext;
                    var sysParas = dbContext.Queryable<SysParam>().ToList();
                    _sysParams = new Dictionary<string, string>();
                    foreach (var conf in sysParas)
                    {
                        _sysParams.Add(conf.Key, conf.Value);
                    }
                }
                return _sysParams;
            }
        }
    }
}
