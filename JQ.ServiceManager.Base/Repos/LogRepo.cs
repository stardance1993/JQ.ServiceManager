﻿using JQ.ServiceManager.Base.Interfaces;
using JQ.ServiceManager.Base.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace JQ.ServiceManager.Base.Repos
{
    public class LogRepo : IRepo<Log>
    {
        public void Delete(Log t)
        {
            throw new NotImplementedException();
        }

        public List<Log> Get(Expression<Func<Log, bool>> expression)
        {
            var context = DataBaseContext.NewContext;
            List<Log> sb = context.Queryable<Log>()
                                    .Where(expression)
                                    .ToList();
            if (sb != null && sb.Any())
            {
                return sb;
            }
            else
            {
                return null;
            }
        }

        public void Insert(Log t)
        {
            throw new NotImplementedException();
        }

        public void Update(Log t)
        {
            throw new NotImplementedException();
        }

    }
}
