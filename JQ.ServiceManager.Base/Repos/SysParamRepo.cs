﻿using JQ.ServiceManager.Base.Interfaces;
using JQ.ServiceManager.Base.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace JQ.ServiceManager.Base.Repos
{
    public class SysParamRepo : IRepo<SysParam>
    {
        public void Delete(SysParam t)
        {
            var context = DataBaseContext.NewContext;
            context.Deleteable<ServiceBase>(t).ExecuteCommand();
        }

        public List<SysParam> Get(Expression<Func<SysParam, bool>> expression)
        {
            var context = DataBaseContext.NewContext;
            List<SysParam> sb = context.Queryable<SysParam>()
                                    .Where(expression)
                                    .ToList();
            if (sb != null && sb.Any())
            {
                return sb;
            }
            else
            {
                return null;
            }
        }

        public void Insert(SysParam t)
        {
            var context = DataBaseContext.NewContext;
            context.Insertable<SysParam>(t).ExecuteCommand();
        }

        public void Update(SysParam t)
        {
            var context = DataBaseContext.NewContext;
            context.Updateable<SysParam>(t).ExecuteCommand();
        }
    }
}
