﻿using JQ.ServiceManager.Base.Interfaces;
using JQ.ServiceManager.Base.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JQ.ServiceManager.Base.Repos
{
    public class ServiceBaseRepo : IRepo<ServiceBase>
    {
        public void Delete(ServiceBase t)
        {
            var context = DataBaseContext.NewContext;
            context.Deleteable<ServiceBase>(t).ExecuteCommand();
        }

        

        public List<ServiceBase> Get(System.Linq.Expressions.Expression<Func<ServiceBase, bool>> expression)
        {
            var context = DataBaseContext.NewContext;
            List<ServiceBase> sb = context.Queryable<ServiceBase>()
                                    .Where(expression)
                                    .ToList();
            if(sb != null && sb.Any())
            {
                return sb;
            }
            else
            {
                return null;
            }
        }

        public void Insert(ServiceBase t)
        {
            var context = DataBaseContext.NewContext;
            context.Insertable<ServiceBase>(t).ExecuteCommand();
        }

        public void Update(ServiceBase t)
        {
            var context = DataBaseContext.NewContext;
            context.Updateable<ServiceBase>(t).ExecuteCommand();
        }
    }
}
