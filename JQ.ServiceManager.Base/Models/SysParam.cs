﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JQ.ServiceManager.Base.Models
{
    public class SysParam
    {
        public int? ID { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }

        public string Group { get; set; }

        public DateTime CreateTime { get; set; }

        public string Note { get; set; }


    }
}
