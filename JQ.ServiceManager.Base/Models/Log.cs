﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JQ.ServiceManager.Base.Models
{
    public class Log
    {

        public int? ID { get; set; }

        public DateTime CreateTime { get; set; }

        public string Type { get; set; }

        public string Message { get; set; }

        public string StackTrace { get; set; }

        public string ServiceName { get; set; }


    }
}
