﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JQ.ServiceManager.Base.Models
{
    public class ServiceManagementObject
    {
        /// <summary>
        /// 服务名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 服务展示名
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// 启动类型
        /// </summary>
        public string StartMode { get; set; }

        /// <summary>
        /// 运行状态
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// 程序路径
        /// </summary>
        public string PathName { get; set; }

        /// <summary>
        /// 启动账户名
        /// </summary>
        public string StartName { get; set; }

    }
}
