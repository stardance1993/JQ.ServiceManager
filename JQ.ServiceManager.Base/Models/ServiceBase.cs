﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JQ.ServiceManager.Base.Models
{
    public class ServiceBase:INotifyPropertyChanged
    {
        public int? ID { get; set; }


        private string _customName;
        public string CustomName
        {
            get { return _customName; }
            set
            {
                _customName = value;
                RaisePropertyChanged(nameof(CustomName));
            }
        }

        private string _serviceName;
        public string ServiceName
        {
            get { return _serviceName; }
            set
            {
                _serviceName = value;
                RaisePropertyChanged(nameof(ServiceName));
            }
        }

        private string _serviceFilePath;

        public string ServiceFilePath
        {
            get { return _serviceFilePath; }
            set
            {
                _serviceFilePath = value;
                RaisePropertyChanged(nameof(ServiceFilePath));
            }
        }

        private string _status;
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                RaisePropertyChanged(nameof(Status));
            }
        }

        private string _isWatch;
        public string IsWatch
        {
            get { return _isWatch; }
            set
            {
                _isWatch = value;
                RaisePropertyChanged(nameof(IsWatch));
            }
        }

        private DateTime dateTime;

        public DateTime CreateTime
        {
            get { return dateTime; }
            set 
            { 
                dateTime = value; 
                RaisePropertyChanged(nameof(CreateTime));
            }
        }

        private string description;

        public string Description
        {
            get { return description; }
            set 
            { 
                description = value; 
                RaisePropertyChanged(nameof(Description));
            }
        }


        private string logRelativePath;

        public string LogRelativePath
        {
            get { return logRelativePath; }
            set
            { 
                logRelativePath = value; 
                RaisePropertyChanged(nameof(LogRelativePath));

            }
        }

        private int triggerHour;

        public int TriggerHour
        {
            get { return triggerHour; }
            set { triggerHour = value; }
        }

        private int triggerMinute;

        public int TriggerMinute
        {
            get { return triggerMinute; }
            set { triggerMinute = value; }
        }

        private string processName;

        public string ProcessName
        {
            get { return processName; }
            set { processName = value; }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName)); 
            }
        }
    }
}
