﻿using JQ.ServiceManager.Base.Models;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JQ.ServiceManager.Base
{
    public class LogApi
    {
        SqlSugarClient logcontext = DataBaseContext.NewContext;
        public void Info(string msg,string stacktrace,ServiceBase item = null)
        {
            logcontext.Insertable(new Log 
            {
                ID = null,
                CreateTime = DateTime.Now,
                Message = msg,
                StackTrace = stacktrace,
                Type = "Info"
            }).ExecuteCommand();
        }

        public void Error(string msg, string stacktrace, ServiceBase item = null)
        {
            logcontext.Insertable(new Log
            {
                ID = null,
                CreateTime = DateTime.Now,
                Message = msg,
                StackTrace = stacktrace,
                Type = "Error"
            }).ExecuteCommand();
        }
    }
}
