﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JQ.ServiceManager.Base
{
    public class ConfigEditor
    {
        public static Dictionary<string, string> GetAppSettings(string exePath)
        {
            Dictionary<string, string> dics = new Dictionary<string, string>();
            Configuration serCfg = ConfigurationManager.OpenExeConfiguration(exePath);
            foreach(var key in serCfg.AppSettings.Settings.AllKeys)
            {
                dics.Add(key, serCfg.AppSettings.Settings[key].Value.ToString());
            }
            return dics;
        }

        public static void UpdateSettings(string exePath,Dictionary<string,string> prepareUpdateSettings)
        {
            Configuration serCfg = ConfigurationManager.OpenExeConfiguration(exePath);
            foreach(var kvPair in prepareUpdateSettings)
            {
                serCfg.AppSettings.Settings[kvPair.Key].Value = kvPair.Value;
            }
            serCfg.Save();
        }
    }
}
