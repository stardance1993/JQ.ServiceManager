﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JQ.ServiceManager.Base
{
    public class DataBaseContext
    {
        public static SqlSugarClient NewContext
        {
            get
            {
                string appWorkspace = AppDomain.CurrentDomain.BaseDirectory;
                string dbFilePath = Path.Combine(appWorkspace, "local.db");
                return new SqlSugarClient(new ConnectionConfig()
                {
                    DbType = DbType.Sqlite,
                    ConnectionString = $"Data Source={dbFilePath};Version=3;",
                    IsAutoCloseConnection = true,
                    InitKeyType = InitKeyType.SystemTable
                }) ;
            }
        }

    }
}
