﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace JQ.ServiceManager.Base
{
    public class ColdDownTool
    {
        private int coldDown;

        /// <summary>
        /// 倒计时秒数
        /// </summary>
        public int ColdDown
        {
            get { return coldDown; }
            set { coldDown = value; }
        }

        private bool isDone;

        /// <summary>
        /// 是否冷却完成
        /// </summary>
        public bool IsDone
        {
            get { return isDone; }
            set { isDone = value; }
        }

        private Timer time;

        public Timer TimerOfColdDown
        {
           get
            {
                return time;
            }
        }

        //冷却计时器
        public ColdDownTool(int seconds)
        {
            IsDone = false;
            ColdDown = seconds;
            time = new Timer();
            time.Interval = seconds * 1000;
            time.Elapsed += (s, e) =>
            {
                this.IsDone = true;
            };
            time.AutoReset = false;
            time.Start();
        }
    }
}
